import urllib,re,zipfile,os, zlib
import wx
from wx.lib.mixins import listctrl
import threading
import StringIO

class MangaList(wx.ListCtrl, listctrl.ListCtrlAutoWidthMixin, listctrl.CheckListCtrlMixin):
    def __init__(self,parent):
        wx.ListCtrl.__init__(self,parent,-1,style=wx.LC_SINGLE_SEL|wx.LC_REPORT)
        listctrl.ListCtrlAutoWidthMixin.__init__(self)
        listctrl.CheckListCtrlMixin.__init__(self)
        self.InsertColumn(0,'',width=25)
        self.InsertColumn(1,'name',width=100)
        self.InsertColumn(2,'status',width=100)
        self.wczytaj_liste()
    
    def wczytaj_liste(self):
        library = open("lib.txt",'r')
        position = library.readlines()
        library.close()
        self.lib = []
        [self.lib.append(position[x].split(';')) for x in range(0,len(position))]
        self.uzupelnij_liste()
        
    def uzupelnij_liste(self):
        for index,i in enumerate(self.lib):
            self.Append(('',i[0],''))
            self.CheckItem(index, bool(i[2]))
    
    @staticmethod
    def odwrotnie_lista(wiersz):
        czy_odwrotna_lista = False
        if len(wiersz) > 3:
            if wiersz[MyMangaGUI.ROW_MANGA_ORDER_INDEX] == "1":
                czy_odwrotna_lista = True
        return czy_odwrotna_lista 
            
        

class MyManga():
    def __init__(self,katalog,adres):
        self.katalog = katalog
        self.adres = adres
        self.domena = re.search('(http://.*?)/',adres,re.DOTALL).group(1)
        self.hosting = MangaHosting(adres)
        self.__pobierz_liste_rozdzialow()
        
    def __pobierz_liste_rozdzialow(self):
        strona = urllib.urlopen(self.adres)
        zawartosc = strona.read()
        strona.close()
        pattern_rozdzialow = self.hosting.pattern_rozdzialow() #pattern dotyczacy rozdzialow
        lista_rozdzialow = re.search(pattern_rozdzialow[0], zawartosc, re.DOTALL).group(1) 
        #drugi pattern rozdzialow
        self.lista_rozdzialow = re.findall(pattern_rozdzialow[1], lista_rozdzialow, re.DOTALL)
        
    def pobierz_adres_stron(self,numer):
        #adres rodzialu
        adres_rozdzialu = self.hosting.adres_rozdzialu(self.lista_rozdzialow[numer])
        strona = urllib.urlopen(adres_rozdzialu)
        zawartosc = strona.read()
        strona.close()
        #pattern stron
        pattern_stron = self.hosting.pattern_stron()
        lista = re.search(pattern_stron[0], zawartosc,re.DOTALL).group(1)
        #drugi pattern stron
        strony = re.findall(pattern_stron[1], lista,re.DOTALL)
        for index, i in enumerate(strony,start=0):
            #adres strony z obrazkiem
            adres_strony = self.hosting.adres_strony(adres_rozdzialu, i)
            strona = urllib.urlopen(adres_strony)
            zawartosc = strona.read()
            if MyManga.__is_gzipped(strona):
                zawartosc = MyManga.__ungzip(zawartosc)
            strona.close()
            pattern_obrazka = self.hosting.pattern_obrazka()
            try:
                strony[index] = re.search(pattern_obrazka, zawartosc,re.DOTALL).group(1)
            except AttributeError:
                print "Nie mozna pobrac obrazka ze strony, moze siadlo kodowanie"
        return strony
    
    def lista_rozdzialow_do_aktualizacji(self):
        lista = []
        for index,i in enumerate(self.lista_rozdzialow):
            nazwa_chaptera = ''.join(i.split('/'))
            if(zipfile.is_zipfile(nazwa_chaptera+'.zip')):
                continue
            lista.append(index)
        return lista
    
    def pobierz_adres_okladki(self):
        strona = urllib.urlopen(self.adres)
        zawartosc = strona.read()
        strona.close()
        pattern_okladki = self.hosting.pattern_okladki()
        adres_okladki = re.search(pattern_okladki,zawartosc).group(1)
        return adres_okladki

    ENCODING_HEADER_KEY = 'Content-Encoding'
    GZIP_ENCODING_VALUE = 'gzip'

    @staticmethod
    def __is_gzipped(req):
        info = req.info()
        return MyManga.ENCODING_HEADER_KEY.lower() in info.keys() and info.get(MyManga.ENCODING_HEADER_KEY) == MyManga.GZIP_ENCODING_VALUE

    @staticmethod
    def __is_successful(req):
        return req.getcode() == 200

    @staticmethod
    def is_image(httpinfo):
        return 'image' in httpinfo.type

    @staticmethod
    def __ungzip(data):
        return zlib.decompress(data, 16 + zlib.MAX_WBITS)
            
            
        
class MyMangaGUI(wx.Frame):
    
    MAX_PAGE_FETCH_TRIES = 10
    ROW_MANGA_NAME_INDEX = 0
    ROW_MANGA_URL_INDEX = 1
    ROW_MANGA_DEFAULT_SELECTION_INDEX = 2
    ROW_MANGA_ORDER_INDEX = 3
    
    def __init__(self,*args,**kwargs):      
        super(MyMangaGUI,self).__init__(style=wx.DEFAULT_FRAME_STYLE, title='MyMangaGUI', *args,**kwargs)
        self.SetSize((525,400))
        self.initUI()
        self.Centre()
        self.Show(True)
        
    def initUI(self):
        self.makeMenuBar()
        self.panel = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour('#ededed')
        self.glownyPanel = wx.FlexGridSizer(1,3,0,5)
        self.prawyPanel = wx.BoxSizer(wx.VERTICAL)

        self.list = MangaList(self)
        self.list.SetSize((250,250))
        self.Bind(wx.EVT_LIST_ITEM_FOCUSED, self.pokazOkladke, self.list)
        
        self.makeButtons()
        
        img = wx.EmptyImage(150,225)
        self.okladka = wx.StaticBitmap(self,wx.ID_ANY,wx.BitmapFromImage(img))
        self.glownyPanel.Add(self.okladka)
        
        self.glownyPanel.Add(self.list,proportion=1,flag=wx.FIXED_MINSIZE)
        self.glownyPanel.Add(self.prawyPanel)
        
        self.panel.Add(self.glownyPanel)
        self.log = wx.TextCtrl(self,-1,style=wx.TE_MULTILINE)
        self.panel.Add(self.log,1,wx.EXPAND)
        self.SetSizer(self.panel)
        
    def deselectAll(self,event):
        for i in range(len(self.list.lib)):
            self.list.CheckItem(i, False)
            
    def selectAll(self,event):
        for i in range(len(self.list.lib)):
            self.list.CheckItem(i, True)

    def __download_cover_job(self,index):
        manga = MyManga(self.list.lib[index][MyMangaGUI.ROW_MANGA_NAME_INDEX], self.list.lib[index][MyMangaGUI.ROW_MANGA_URL_INDEX])
        buf = urllib.urlopen(manga.pobierz_adres_okladki()).read()
        sbuf = StringIO.StringIO(buf)
        img = wx.ImageFromStream(sbuf).Scale(150,225).ConvertToBitmap()
        wx.CallAfter(self.okladka.SetBitmap,img)
    
    def pokazOkladke(self,event):
        index = event.m_itemIndex
        thread = threading.Thread(target=self.__download_cover_job, args=(index,))
        thread.start() 
        
        
    def makeMenuBar(self):
        menu_bar = wx.MenuBar()
        self.SetMenuBar(menu_bar)
        plik_menu_bar = wx.Menu()
        menu_bar.Append(plik_menu_bar,'Plik')
        
    def makeButtons(self):
        selectButton = wx.Button(self,label="Select All")
        selectButton.Bind(wx.EVT_BUTTON, self.selectAll, selectButton)
        deselectButton = wx.Button(self,label="Deselect All")
        deselectButton.Bind(wx.EVT_BUTTON, self.deselectAll, deselectButton)
        
        update = wx.Button(self,label='Update')
        update.Bind(wx.EVT_BUTTON, self.OnUpdate, update)
        
        self.prawyPanel.Add(update)
        self.prawyPanel.Add(selectButton)
        self.prawyPanel.Add(deselectButton)
        
    def OnUpdate(self,event):
        thread = threading.Thread(target=self.update, args=())
        thread.start()
        
    def update(self):
        wx.CallAfter(self.log.Clear)
        for index,i in enumerate(self.list.lib):
            wx.CallAfter(self.log.AppendText,i[0])
            if self.list.IsChecked(index):
                wx.CallAfter(self.printf," Zaznaczone\n")
                wx.CallAfter(self.list.SetStringItem,index,2,"sprawdzanie")
                manga = MyManga(i[MyMangaGUI.ROW_MANGA_NAME_INDEX], i[MyMangaGUI.ROW_MANGA_URL_INDEX])
                if not os.path.isdir(manga.katalog):
                    try:
                        os.mkdir(manga.katalog)
                    except OSError:
                        self.printf('Nie mozna utworzyc folderu')
                os.chdir(manga.katalog)
                wx.CallAfter(self.printf,"Lista dla "+manga.katalog+":\n")
                manga_lista_rozdzialow = manga.lista_rozdzialow_do_aktualizacji()
                if MangaList.odwrotnie_lista(i):
                    manga_lista_rozdzialow.reverse()
                for c in manga_lista_rozdzialow:
                    nazwa_chaptera = ''.join(manga.lista_rozdzialow[c].split('/'))
                    wx.CallAfter(self.printf,"\tChapter "+nazwa_chaptera+".zip\n")
                    wx.CallAfter(self.list.SetStringItem,index,2,"pobieranie")
                    self.pobierz_rozdzial(nazwa_chaptera, manga.pobierz_adres_stron(c))
                os.chdir('..')
                if len(manga_lista_rozdzialow) > 0:
                    wx.CallAfter(self.list.SetStringItem,index,2,"zaktualizowane")   
                else:
                    wx.CallAfter(self.list.SetStringItem,index,2,"nic nowego")
            else:
                wx.CallAfter(self.printf," Odznaczone\n")
                
    def pobierz_rozdzial(self,nazwa_archiwum,strony):
        archiwum = zipfile.ZipFile(nazwa_archiwum+'.zip','w')
        for index,i in enumerate(strony, start=1):
            nazwa_strony = str(index).zfill(len(str(len(strony))))+os.path.splitext(i)[1]
            if self.__pobierz_strone(i, nazwa_strony):
                archiwum.write(nazwa_strony)
                os.remove(nazwa_strony)
                wx.CallAfter(self.printf,"\t\tPobrano strone "+str(index)+"/"+str(len(strony))+"\n")
            else:
                archiwum.close()
                os.remove(archiwum.filename)
                raise IOError("limit exceeded","tries limit exceeded")
        archiwum.close()
        
    def printf(self,output):
        self.log.AppendText(output)
    
    def __pobierz_strone(self,url,nazwa_strony):
        for proba in range(1,MyMangaGUI.MAX_PAGE_FETCH_TRIES+1):
            try:
                _,info = urllib.urlretrieve(url,nazwa_strony)
                if not MyManga.is_image(info):
                    raise IOError("Nie odebrano obrazu (%s)" % info.type)
            except IOError as e:
                self.printf("\t\t\t-> Proba {0} [{1}]\n".format(proba,e.message))
            else:
                return True
        return False

        
class MangaHosting():
    USER_AGENT_MOCK_NAME = 'Mozilla/5.0 (Windows NT 6.1; rv:39.0) Gecko/20100101 Firefox/39.0'  
    
    def __init__(self,adres):
        self.adres = adres
        self.domena = re.search('(http://.*?)/',adres,re.DOTALL).group(1)
        self.hosting = self.__rozpoznaj_hosting()
    def __rozpoznaj_hosting(self):
        return {
                'http://www.mangapanda.com':1,
                'http://mangafox.me':2,
        'http://www.mangahere.com':3
                }.get(self.domena,None)
    def pattern_rozdzialow(self):       
        return {
                1:('<div\s*?id="chapterlist"\s*?>(.+?)</table>','<a\s*?href="(.+?)"\s*?>'),
                2:('<div\s*?id="chapters".*?>(.+?)<div id="discussion"','<a\s*?href="'+self.domena+'(.+?)/\d*?\.html".*?>.*?</a>'),
                3:('<ul>(.*?)</ul>','<a\s+?class=".*?"\s+?href="'+self.domena+'(.*?)"\s*?>')
                }.get(self.hosting,None)
    def adres_rozdzialu(self,c):
        return self.domena+c
    def pattern_stron(self):
        return {
                1:('<\s*?div\s*?id=\"selectpage\"\s*?>(.*?)<\s*?/div\s*?>','<\s*option\s*value="(.+?)".*?>'),
                2:('<\s*?select.*?class="m">(.*?)<\s*?/select\s*?>','<\s*option\s*value="(.+?)".*?>\d.*?</'),
        3:('<select\s+?class.*?>(.*?)</select>','<option value="'+self.domena+'(.*?)"')
                }.get(self.hosting,None)
    def adres_strony(self,c,i):
        return {
                1:self.domena+i,
                2:c+"/"+i+".html",
        3:self.domena+i
                }.get(self.hosting,None)
    def pattern_obrazka(self):
        return {
                1:'<\s*img.*?src="(.*?)".*?/>',
                2:'<\s*img.*?src="(.*?)".*?/>',
        3:'<\s*?img.*?src="(.*?)\?'
                }.get(self.hosting,None)
    def pattern_okladki(self):
        return {
                1:'<div id="mangaimg".+?src="(.+?)"',
                2:'<div class="cover">[\w\W]+?src="(.+?)"',
                3:'<div class="manga_detail">[\w\W]+?src="(.+?)"'
        }.get(self.hosting,None)
        
    @staticmethod
    def ukryj_nazwe_przegladarki(user_agent=USER_AGENT_MOCK_NAME):
        urllib.URLopener.version = user_agent
        
if __name__ == "__main__":
    print "Uruchom..."
    MangaHosting.ukryj_nazwe_przegladarki()
    apka = wx.App()
    MMG = MyMangaGUI(None)
    apka.MainLoop()
